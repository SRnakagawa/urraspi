#!/usr/bin/env python
import rospy
from sensor_msgs.msg import Joy
import time
import grovepi

class UrRasPiJoy:
    def __init__(self):
        self.pub = rospy.Publisher('urraspijoystick',Joy,queue_size=10)
        rospy.init_node('urraspijoy',anonymous=True)
        self.rate = rospy.Rate(10)
        for i in range(4):
            grovepi.pinMode(i,"INPUT")
        self.calb = [0.0,0.0,0.0,0.0]
        for i in range(20):
            for j in range(4):
                self.calb[j] += grovepi.analogRead(j)/20.0
        print self.calb

    def talker(self):
        while not rospy.is_shutdown():
            joy_msg = Joy()
            joy_msg.header.stamp = rospy.Time.now()
            list = []
            for i in range(4):
                list.insert(i,grovepi.analogRead(i))
                list[i] = (list[i]-self.calb[i])/(self.calb[i]/2.0)
            joy_msg.axes = list
            joy_msg.buttons = [0,0,0,0,0,0,0,0,0,0]
            self.pub.publish(joy_msg)
            self.rate.sleep()


if __name__ == '__main__':
    test = UrRasPiJoy()
    test.talker()
