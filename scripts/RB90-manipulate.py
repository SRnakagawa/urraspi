#!/usr/bin/env python

import rospy
from sensor_msgs.msg import Joy
import time
import pigpio

class RBman:
    def __init__(self):
        rospy.init_node('rbman',anonymous=True)
        self.subscriber = rospy.Subscriber("/joy",Joy,self.callback)
        self.pi = pigpio.pi()
        self.con=0
        self.param=1400
        rospy.spin()
    def callback(self,ros_data):
        print ros_data.buttons[3]
        if(ros_data.buttons[3]==1 and self.con==0):
            self.pi.set_servo_pulsewidth(4,1400)
            self.con=1
            time.sleep(0.1)
        elif(ros_data.buttons[3]==1 and self.con==1):
            self.pi.set_servo_pulsewidth(4,1790)
            self.con=0
            time.sleep(0.1)

if __name__=='__main__':
    rbman = RBman()
